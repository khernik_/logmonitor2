(function() {

    var Tools = (function() {
        var ajax = function() {
            var xmlhttp;
            return {
                createXmlhttp: function() {
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                },
                onreadystatechange: function(action) {
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            action(xmlhttp.responseText);
                        }
                    };
                },
                execute: function(action, url, data) {
                    this.createXmlhttp();
                    this.onreadystatechange(action);
                    xmlhttp.open("POST", url, true);
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send(data);
                }
            };
        };
        return {
            ajax: ajax
        };
    })();

    var HTML = (function() {
        var getContainer = function(wrapper) {
            var el = wrapper.getElementsByTagName('tbody')[0];
            el.innerHTML = '';
            return el;
        };
        var createRow = function() {
            return document.createElement('tr');
        };
        var createCell = function(value, row) {
            var td = document.createElement('td');
            td.innerHTML = value;
            row.appendChild(td);
            return td.innerHTML;
        };
        return {
            getContainer: getContainer,
            createRow: createRow,
            createCell: createCell
        };
    })();

    var Log = (function() {
        var numberOfIntervals = 1,
            excludedFromLoading = [],
            logContainers = [];
        var init = function() {
            logContainers = document.getElementsByClassName('singleLog');
            setInterval(load, 1000);
            $('.stopReadingMessage').html('Reading in progress :)');
            $('.stopReadingMessage').css('color', 'green');
        };
        var load = function() {
            Tools.ajax().execute(function(response) {
                try {
                    var outputs = JSON.parse(response);
                } catch (e) {
                    return false;
                }

                var i = 0;
                for (var path in outputs) {
                    if (helper.isLogExcludedFromLoadingData(path)) {
                        continue;
                    }

                    var container = HTML.getContainer(logContainers[i]),
                        numberOfLogsFromLastSeconds = 0;
                    for (var line in outputs[path]) {
                        var row = HTML.createRow();

                        var line = outputs[path][line];

                        var date = HTML.createCell(line.match(new RegExp('(.*?) '))[1], row);
                        var time = HTML.createCell(line.match(new RegExp(date + ' (.*?) '))[1], row);
                        var message = HTML.createCell(line.match(new RegExp(time + '.*?\\*.*? (.*?), client'))[1], row);
                        var client = HTML.createCell(line.match(new RegExp(message + ', client: (.*?),'))[1], row);
                        var server = HTML.createCell(line.match(new RegExp(client + ', server: (.*?),'))[1], row);
                        var request = HTML.createCell(line.match(new RegExp(server + ', request: "(.*?)",'))[1], row);
                        var upstream = HTML.createCell(line.match(new RegExp(request + '", upstream: "(.*?)",'))[1], row);
                        var host = HTML.createCell(line.match(new RegExp(upstream + '", host: "(.*?)"'))[1], row);

                        if (helper.isLogFromLastSeconds(time, numberOfIntervals)) {
                            numberOfLogsFromLastSeconds++;
                        }

                        //row.style.display = "table-row";
                        container.appendChild(row);
                    }

                    helper.fillGetLatestLog(date + ' ' + time, i);
                    helper.fillNumberOfLogsFromLastSeconds(numberOfLogsFromLastSeconds, i);
                    helper.attachStopReadingEvent(path);

                    i++;
                }
                afterLoading();
            }, 'ajaxGetLatestLogs', '');
        };
        var afterLoading = function() {
            $('#timeToRequest').fadeIn(200).fadeOut(200);

            var numberOfIntervalsEl = document.getElementsByClassName('numberOfIntervals');
            for (var i = 0; i < numberOfIntervalsEl.length; i++) {
                numberOfIntervalsEl[i].innerHTML = numberOfIntervals;
            }

            if (numberOfIntervals < 60) numberOfIntervals++;
        };
        var helper = (function() {
            var isLogExcludedFromLoadingData = function(path) {
                return (excludedFromLoading[path] !== undefined) ? true : false;
            };
            var isLogFromLastSeconds = function(logTime, startTime) {
                var currTime = Date.now();

                // convert time to timestamp
                var ss = logTime.split(":");
                var logTime = new Date();
                logTime.setHours(ss[0]);
                logTime.setMinutes(ss[1]);
                logTime.setSeconds(ss[2]);

                if (currTime < logTime.valueOf() + startTime*1000) {
                    return true;
                } else {
                    return false;
                }
            };
            var fillGetLatestLog = function(text, i) {
                var el = document.getElementsByClassName('timeOfLatest');
                el[i].innerHTML = text;
                return true;
            };
            var fillNumberOfLogsFromLastSeconds = function(number , file) {
                var el = document.getElementsByClassName('numberOfLogs');
                el[file].innerHTML = number;
                if (number > 0) {
                    el[file].style.color = "red";
                    el[file].style.fontWeight = "bold";
                }
                return true;
            };
            var attachStopReadingEvent = function(file) {
                $('.stopReading')
                    .off('click')
                    .on('click', function() {
                        if ($(this).html() == 'Stop reading') {
                            excludedFromLoading[file] = 1;
                            $(this).html('Start reading');
                            $(this).parent().parent().find('.stopReadingMessage').html('Reading stopped...');
                            $(this).parent().parent().find('.stopReadingMessage').css('color', 'red');
                        } else {
                            excludedFromLoading[file] = undefined;
                            $(this).html('Stop reading');
                            $(this).parent().parent().find('.stopReadingMessage').html('Reading in progress :)');
                            $(this).parent().parent().find('.stopReadingMessage').css('color', 'green');
                        }
                    });
                return true;
            };
            return {
                isLogExcludedFromLoadingData: isLogExcludedFromLoadingData,
                isLogFromLastSeconds: isLogFromLastSeconds,
                fillGetLatestLog: fillGetLatestLog,
                fillNumberOfLogsFromLastSeconds: fillNumberOfLogsFromLastSeconds,
                attachStopReadingEvent: attachStopReadingEvent
            };
        })();
        return {
            init: init,
            helper: helper
        };
    })();

    Log.init();

})();
